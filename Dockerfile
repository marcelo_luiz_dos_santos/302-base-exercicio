FROM openjdk:8-alpine
WORKDIR /Api-Investimentos
COPY target/Api-Investimentos*.jar ./investimentos.jar
CMD ["java", "-jar", "investimentos.jar"]
